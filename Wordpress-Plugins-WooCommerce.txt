﻿AJAX FREE NAVIGATION
Allows you to filter the products in the Shop page without reloading the page.
http://wordpress.org/plugins/yith-woocommerce-ajax-navigation/


AJAX PREMIUM NAVIGATION
Allows you to filter the products in the Shop page without reloading the page.
http://www.woothemes.com/products/ajax-layered-navigation/


ANALYTICS SEGMENT.IO
Integrate the power of Segment.io’s Analytics.js into your WordPress WooCommerce storefront.
http://codecanyon.net/item/analyticsjs-segmentio-for-woocommerce/5141415


BITCOIN PAYMENT
Plugin for connecting Woocommerce with Bitpay.com
https://github.com/bitpay/woocommerce-plugin


BOOKING AND APPOINTMENT
This plugin lets you capture the Booking Date & Booking Time for each product thereby allowing your WooCommerce store to effectively function as a Booking & Appointment system. 
http://www.tychesoftwares.com/store/premium-plugins/woocommerce-booking-plugin


BRANDING AND BRANDS
Brands extension for WooCommerce allows you to create brands for your shop; each brand can be named, described and assigned an image. 
http://www.woothemes.com/products/brands/


CHECKOUT MANAGER
This plugin gives you a vast amount of capabilities to manage your products on checkout such as removing fields that you do not need, removing the required attribute rendering the field optional to customer or even perhaps adding more fields to the checkout page.
http://wordpress.org/plugins/woocommerce-checkout-manager/screenshots/


CHECKOUT TOOLTIPS
Gives you the ability to add custom tooltips to the default fields on the checkout page of WooCommerce.
http://codecanyon.net/item/woocommerce-checkout-tooltips/5123514


DIRECT CHECKOUT
You can skip shopping cart page and implement add to cart button redirect to checkout page.
http://terrytsang.com/shop/shop/woocommerce-direct-checkout/


FACEBOOK SHARE and LIKE FROM WC
A WooCommerce plugin that implements facebook share and like button on product page with flexible options.
http://wordpress.org/plugins/woocommerce-facebook-share-like-button/


FAQ FOR PRODUCTS
Extends WooCommerce to allow for the asking, answering, and viewing of FAQs in a similar experience as on eBay.
http://wordpress.org/plugins/woocommerce-product-faqs/


FILTER PRODUCT AJAX
WooCommerce AJAX Layered Navigation adds advanced product filtering to your WooCommerce shop. It provides rich Interface for users to filter products by custom attributes, custom taxonomies, hierarchical categories, custom post meta fields, WooCommerce products meta and… at real time.
http://codecanyon.net/item/woocommerce-ajax-product-filter-wordpress-plugin/4640387


FOLLOW UP EMAILS
build email templates to send single emails to customers, and automate your email marketing to increase the value of your existing customer base
http://www.woothemes.com/products/follow-up-emails/


FRONT END INVENTORY
Publish a full inventory on a frontend page with a simple shortcode
http://wordpress.org/plugins/woocommerce-frontend-inventory/screenshots/


GENESIS CONNNECT
Connect WC to your genesis framework
http://wordpress.org/plugins/genesis-connect-woocommerce/


GOOGLE ADWORDS TAG
Google AdWords conversions in conjunction with WooCommerce. Whereas other available plugins inject a static AdWords tracking tag, this plugin is dynamic and enables the tracking code to also measure the total value of the transaction.
http://wordpress.org/plugins/woocommerce-google-adwords-conversion-tracking-tag/


IMAGES FOR EVERY VARIATION
Multiple Images per Variation for WooCommerce allows you to assign images to particular variations of a WooCommerce product.
http://codecanyon.net/item/multiple-images-per-variation-for-woocommerce/2867927


MANAGE PRODUCTS
Productivity gains with WP e-Commerce & WooCommerce store administration. Manage products, variations, orders and customers reliably
http://wordpress.org/plugins/smart-manager-for-wp-e-commerce/


NEW PRODUCT BADGE
Displays a 'new' badge on WooCommerce products published in the last x days.
http://wordpress.org/plugins/woocommerce-new-product-badge/


ORDER CUSTOM DATA
extend WC orders with custom data
http://wordpress.org/plugins/woocommerce-custom-order-data/


PHOTOS PRODUCT TAB
Allows you to display all the images you have attached to a product in a new tab on the single product page.
http://wordpress.org/plugins/woocommerce-photos-product-tab/


PRODUCT DOCUMENTS
Product Documents can display documentation below the product short description, in the sidebar using our widget, or anywhere you can use shortcodes!
http://www.woothemes.com/products/product-documents/


PRODUCT VENDORS
Turn your store into a multi-vendor marketplace (such as Etsy or Creative Market)
http://docs.woothemes.com/document/product-vendors/


RADIO BUTTONS FOR VARIATIONS
Light-weight plugin that converts your Woocommerce variations in to radio buttons.
http://wordpress.org/plugins/woocommerce-radio-buttons/


REBRANDING
Branding extension transforms WooCommerce into a whitelabel solution, allowing you to replace all WooCommerce branding with your own.
http://www.woothemes.com/products/woocommerce-branding/


SOCIAL COUPON
WooCommerce extension that allows you to easily add a social coupon system to your site that allows users to get instant discounts for sharing your pages.
http://codecanyon.net/item/social-coupon-for-wordpress/3417466/?ref=dalecom


TAB MANAGER
Tab Manager gives you complete control over your product page tabs, allowing you to easily create new tabs for products, share tabs among multiple products, reorder tabs using a visual drag-and-drop interface, and more
http://www.woothemes.com/products/woocommerce-tab-manager/


TICKETS
WooCommerce Tickets add-on for The Events Calendar/Events Calendar PRO, taking control of ticket sales for an upcoming event has never been easier.
http://tri.be/shop/wordpress-wootickets/


UPLOAD WITH PAYMENT
Allow customers to upload a file, Once a WooCommerce order has been successfully made by the buyer / customer, a file upload screen will appear on the order details page.
http://wordpress.org/plugins/woocommerce-pay-to-upload-modification/


VARIATIONS TABLE
WooCommerce Variations Table lets you display a list of all your variations in an easy to read table format.
http://ignitewoo.com/woocommerce-extensions-plugins/woocommerce-variations-table/


VAT OIB NUMBER PROCCESS
Allow valid EU businesses the choice of paying tax at your store
http://www.woothemes.com/products/eu-vat-number/


VIDEO PRODUCT TAB
Add a Video to the Product page. An additional tab is added on the single products page to allow your customers to view the video you embeded.
http://wordpress.org/plugins/woocommerce-video-product-tab/


WAITLIST
Waitlist extension lets you track demand for out-of-stock items, making sure your customers feel informed, and therefore more likely to buy.
http://www.woothemes.com/products/woocommerce-waitlist/


WISHLIST
YITH WooCommerce Wishlist add all Wishlist features to your website. Needs WooCommerce to work.
http://wordpress.org/plugins/yith-woocommerce-wishlist/


ZENDESK CONNECT
Easy connect WooCommerce with Zendesk, directly create new Zendesk end users and organizations from new customers
http://wordpress.org/plugins/woocommerce-zendesk-connect/


ZERO PRICE
Add a custom message when your product doesn’t have a price, a simple yet missing feature of WooCommerce that offers a better user experience by keeping customers up to date and in the loop.
http://codecanyon.net/item/custom-zero-price-for-woocommerce/3787102/?ref=dalecom


ZOOM MAGNIFIER
YITH WooCommerce Zoom Magnifier is a WordPress plugins that enables you to add a zoom effect to product images.
http://wordpress.org/plugins/yith-woocommerce-zoom-magnifier/